[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Overview
This repository is part of the [ROS-Industrial](https://wiki.ros.org/Industrial) program.

The Fanuc Post Processor goal is to allow to create LS programs for Fanuc robots from ROS trajectories.

# Dependencies

## wstool
Install [wstool](https://wiki.ros.org/wstool).

## rosdep
Install, initialize and update [rosdep](https://wiki.ros.org/rosdep).

# Compiling
Create a catkin workspace and clone the project:

```bash
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone https://gitlab.com/arulfrances/fanuc_post_processor.git
cd ..
wstool init src src/fanuc_post_processor/fanuc_post_processor.rosinstall
```

## Resolve ROS dependencies
```bash
rosdep install --from-paths src --ignore-src --rosdistro $ROS_DISTRO -y
```

## Compile
```bash
catkin_make
```

# Testing
The package includes a test that showcases the usage of the library: [test/program.cpp](test/program.cpp)

# Troubleshooting upload
If you have trouble uploading programs to your robots, here are simple tests to help you find what's wrong:

- Make sure you can ping the robot controller in a terminal: `ping 192.168.1.1`
- Make sure you have ASCII Upload; in your favorite web browser go to the IP address of the robot and check for the Ascii Upload option (R507) on the `Summary Configuration/Status` page
- Make sure you can connect to the FTP (using your credentials if applicable):
```bash
$ ftp
ftp> open 192.168.1.1
Connected to 192.168.1.1.
220 R-30iB FTP server ready. [ArcTool V8.20P/24]
Name (192.168.100.200:dell):
230 User logged in [NORM].
Remote system type is UNKNOWN.
```
- Make sure you can upload a simple program, `ROS_TP_PROGRAM.ls`
```bash
/PROG ROS_TP_PROGRAM
/ATTR
COMMENT = "ROS generated";
PROTECT = READ_WRITE;
DEFAULT_GROUP = 1,*,*,*,*;
/MN
:  !This is a ROS generated TP program;
:  DO[5]=ON;
:  WAIT  0.5(sec);
:  DO[5]=OFF;
/POS
/END
```
Upload it on with the FTP:

```bash
ftp> put ./ROS_TP_PROGRAM.ls
local: ./ROS_TP_PROGRAM.ls remote: ./ROS_TP_PROGRAM.ls
200 PORT command successful.
150 ASCII data connection.
226 ASCII Transfer complete.
215 bytes sent in 0.00 secs (7240.0 kB/s)
```

If the upload loops/fails, check the Teach Pendant alarm history to get details on the error and open an issue.
