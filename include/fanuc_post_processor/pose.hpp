#ifndef FANUC_POST_PROCESSOR_POSE_HPP
#define FANUC_POST_PROCESSOR_POSE_HPP

#include <Eigen/Dense>
#include <fanuc_post_processor/group.hpp>
#include <industrial_robot_angle_conversions/poses.hpp>
#include <ros/ros.h>
#include <tuple>

namespace fanuc_post_processor
{

class FanucPose
{
public:
  /**
   * Fanuc movement types
   * Other types are not supported (circular etc.)
   */
  enum MovementType
  {
    JOINT, LINEAR
  };

  /**
   * Fanuc speed types
   * Other types are not supported (SEC, MSEC, MAX_SPEED)
   */
  enum SpeedType
  {
    MM_SEC, CM_SEC, M_SEC, MM_MIN, CM_MIN, M_MIN, DEG_SEC, PERCENTAGE
  };

  /**
   * Robot configuration
   */
  enum class FlipNonFlip
  {
    Flip,  //!< Flip
    NonFlip  //!< NonFlip
  };

  /**
   * Robot configuration
   */
  enum class Elbow
  {
    Up,
    Down
  };

  /**
   * Robot configuration
   */
  enum class BackFront
  {
    Back,
    Front
  };

  /**
   * Robot configuration
   */
  using Config = std::tuple<FlipNonFlip, Elbow, BackFront>;

private:
  std::string
  movementTypeToString() const;

  std::string
  speedTypeToString() const;

  std::string
  configurationToString() const;

public:

  /**
   * Construct a Fanuc pose
   * @param extended_axes 1 or more axis added to group 1
   * @param extra_groups group 2, 3 etc.
   */
  FanucPose(const std::vector<FanucAxis> extended_axes = {},
            const std::vector<FanucGroup> extra_groups = {});

  virtual ~FanucPose();

  /**
   * Checks if poses have the same groups/axes per groups
   * Does NOT check that user frame, user tool, pose are identical!
   *
   * @param other
   * @return true if poses have the same groups/axes per groups
   */
  bool hasSameGroupsAxes(const FanucPose &other) const;

  /**
   * Check if all units/fields are valid
   * @return true if valid, false otherwise
   */
  bool isValid() const;

  /**
   * @return a string containing the /MN program line corresponding to this pose
   */
  std::string getMainLineString() const;

  /**
   * @return a string containing the /POS program lines corresponding to this pose
   */
  std::string getPoseLinesString() const;

  /**
   * Modifies the first 6 axes values of the group 1 to the specified pose
   * @param pose specified pose
   */
  void setRobotPose(const Eigen::Isometry3d &pose);

  // If true: this pose is only a duplicate of an existing pose in the program
  // The existence will be verified when adding the pose
  bool is_pose_id_ = false;
  unsigned pose_id_ = 1; // Unique within a Fanuc program. Cannot be zero
  MovementType move_type_ = MovementType::LINEAR;
  double speed_ = 10;
  SpeedType speed_type_ = SpeedType::CM_MIN;
  // CNT = 0 == Fine
  // CNT 1-100 is smooth motion
  unsigned cnt_ = 100;
  unsigned acc_ = 100;
  bool speed_register_ = false; // If true, speed will be converted to unsigned and used as a register number
  Config configuration_;
  int flipnonflip_rotations_ = 0, elbow_rotations_ = 0, backfront_rotations_ = 0;
  std::vector<FanucGroup> groups_;
  std::string option_; // Time before, time after, offset PR etc...
};

}

#endif
