#ifndef FANUC_POST_PROCESSOR_HPP
#define FANUC_POST_PROCESSOR_HPP

#include <algorithm>
#include <fanuc_post_processor/commands.hpp>
#include <fanuc_post_processor/curlite.hpp>
#include <fanuc_post_processor/pose.hpp>
#include <fstream>
#include <limits>
#include <ros/ros.h>
#include <stdexcept>
#include <string>

namespace fanuc_post_processor
{
/**
 * @file post_processor.hpp
 * @brief Fanuc post-processor
 * @author Victor Lamoine - Institut Maupertuis
 * @date Project started in March 2016
 */

/** @mainpage Fanuc post-processor library
 * Please read [InstitutMaupertuis/fanuc_post_processor](https://gitlab.com/InstitutMaupertuis/fanuc_post_processor)
 */

/** @brief FanucPostProcessor class */
class FanucPostProcessor
{
public:
  enum Permissions
  {
    READ_WRITE, READ
  };

public:
  /** @brief Default Constructor */
  FanucPostProcessor();

  ~FanucPostProcessor();

  /**
   * Writes the program content into a string.
   * @param[out] output_programs this vector of pairs (program name, program) contains the programs
   * @param[in] lines_per_program the maximum number of lines in a program
   * @return The number of programs generated, zero meaning error
   * Multiple programs are generated depending on the lines_per_program limit, if set to
   * 0 only one program will be generated
   */

  using ProgramName = std::string;
  using ProgramText = std::string;
  using FanucProgram = std::pair<ProgramName, ProgramText>;
  using FanucPrograms = std::vector<FanucProgram>;
  unsigned generatePrograms(FanucPrograms &output_programs,
                            const unsigned lines_per_program = 0);

  void clearProgram();

  void useLineNumbers(bool line_numbers);

  /**
   * Upload a LS/TP program to a Fanuc robot FTP
   * @param[in] programs contains the program names and content to be uploaded,
   * @param[in] ip_address the IP address of the Fanuc FTP
   * @param[in] port_number the port number of the FTP
   * @param[in] username should be empty if anonymous login
   * @param[in] password should be empty if anonymous login
   */
  void
  uploadToFtp(const FanucPrograms programs,
              const std::string ip_address,
              const std::string port_number = "21",
              const std::string username = "",
              const std::string password = "");

  /**
   * Allows to set the program name in the header
   * @param[in] name without the file extension (.ls)
   * @return True if successful, false otherwise
   * @note The program name maximum length is 36 characters
   */
  void setProgramName(ProgramName name);

  /**
   * Allows to set the program comment
   * @param[in] comment
   * @return True if successful, false otherwise
   * @note The comment string maximum size is 16, characters after are truncated by the Fanuc converter
   */
  void setProgramComment(const std::string comment);

  /**
   * This is computed by the Fanuc ASCII upload compiled, it is not mandatory to set it
   * @param[in] size
   */
  void setMemorySize(const unsigned size);

  /**
   * Set the program permissions on the robot
   * @param[in] perms
   */
  void setPermissions(const FanucPostProcessor::Permissions perms);

  /**
   * Allows to specify an application for specific instructions (eg: ArcTool)
   * @param[in] appl
   */
  void setApplicative(const std::string appl);

  /**
   * Add a Fanuc pose
   * @param[in] pose
   * @note The pose_id_ member of the pose will be set by this function, first pose has ID == 1
   */
  void
  appendPose(FanucPose &pose);

  /**
   * Add a Fanuc pose by using an existing ID
   * @param[in] pose the pose_id_ field will be used to determine to which pose it refers
   * @return True if successful, false otherwise
   * @note Look at appendPose to know how ID are set
   * @note The groups_ field will be ignored and cleared
   */
  void
  appendPoseId(FanucPose &pose);

  /**
   * Add a comment in the program, starting with an exclamation point
   * @param[in] comment
   */
  void
  appendComment(const std::string comment);

  /**
   * Add an empty line
   */
  void
  appendEmptyLine();

  /**
   * Set a digital output value to either TRUE or FALSE
   * @param[in] digital_out_id
   * @param[in] state
   */
  void
  appendDigitalOutput(const unsigned digital_out_id,
                      const bool state);

  /**
   * Set a digital output pulse time
   * @param[in] digital_out_id
   * @param[in] pulse_time
   * @note The pulse time is in seconds
   */
  void
  appendDigitalOutput(const unsigned digital_out_id,
                      const double pulse_time);

  /**
   * Add a waiting time in the program
   * @param[in] time
   * @note The duration is in seconds
   */
  void
  appendWait(const double time);

  /**
   * Add a track DPM
   * @param[in@ number
   */
  void
  appendTrackDPM(const unsigned number);

  /**
   * Add a track end
   */
  void
  appendTrackEnd();

  /**
   * Wait for a Digital Input to be triggered
   * @param[in] digital_in_id
   * @param[in] state
   */
  void
  appendWait(const unsigned digital_in_id,
             const bool state);

  /**
   * Wait for a Group Input to reach a certain value
   * @param[in] group_id
   * @param[in] value
   */
  void
  appendWait(const unsigned group_id,
             const unsigned value);

  /**
   * Reset a timer
   * @param[in] id
  */
  void
  appendTimerReset(const unsigned id);

  /**
   * Start a timer
   * @param[in] id
  */
  void
  appendTimerStart(const unsigned id);

/**
   * Stop a timer
   * @param[in] id
  */
  void
  appendTimerStop(const unsigned id);

  /**
   * Append a UserFrame instruction. From 0 to 9
   * @param[in] uf_id
   */
  void
  appendUFrame(const unsigned uf_id);

  void 
  appendUFrame(const unsigned uf_id, const unsigned g_id);

  /**
   * Append a UserTool instruction. From 1 to 9
   * @param[in] ut_id
   */
  void
  appendUTool(const unsigned ut_id);

  void
  appendUTool(const unsigned ut_id, const unsigned g_id);
  /**
   * Set a group output to a value
   * @param[in] id
   * @param[in] value
   */
  void
  appendGroupOutput(const unsigned id,
                    const unsigned value);

  /**
   * Set a register to a value
   * @param[in] r_id
   * @param[in] value
   */
  void
  appendSetRegister(const unsigned r_id,
                    const double value);

  /**
   * Set a register to a value
   * @param[in] r_id
   * @param[in] value
   */
  void
  appendSetRegister(const unsigned r_id,
                    const unsigned value);

  /**
   * Assign a register to an other register value
   * @param[in] r1_id
   * @param[in] r2_id
   @note R1 = R2
   */
  void
  appendSetRegisterToRegister(const unsigned r1_id,
                              const unsigned r2_id);

  /**
   * Set a flag to a boolean value
   * @param[in] f_id
   * @param[in] value
  */
  void
  appendSetFlag(const unsigned f_id,
                const bool value);

  /**
   * Call a program
   * @param[in] program_name
   * @warning Transferring the program on the robot may fail if the program specified does not exist
   */
  void
  appendCall(const std::string program_name);

  /**
   * Run a program
   * @param[in] program_name
   * @warning Transferring the program on the robot may fail if the program specified does not exist
   */
  void
  appendRun(const std::string program_name);

  /**
   * Add a label, each label ID can only be added once
   * @param[in] id
   * @note Does NOT work if multiple programs are generated
   */
  void
  appendLabel(const unsigned id);

  /**
   * Jump to an existing label, fails if the label doesn't exist
   * @param[in] id
   * @note Does NOT work if multiple programs are generated
   */
  void
  appendJumpLabel(const unsigned id);

  /**
   * Start data monitor
   * @param[in] id
   */
  void
  appendDataMonitorStart(const unsigned id);

  /**
   * Stops data monitor
   */
  void
  appendDataMonitorStop();

  /**
   * Append a for loop, start and stop condition must be valid strings
   * @param reg_id register to store the loop increment
   * @param start value
   * @param stop value
   * @note Does NOT work if multiple programs are generated
   */
  void appendFor(const unsigned reg_id,
                 const std::string start,
                 const std::string stop);

  /**
   * For loop in an integer range, eg: FOR R[1]=1 TO 8;
   * @param reg_id register to store the loop increment
   * @param start value
   * @param stop value
   * @note Does NOT work if multiple programs are generated
   */
  void appendFor(const unsigned reg_id,
                 const unsigned start,
                 const unsigned stop);

  /**
   * Stop a for loop, fails if no for loop was opened
   * @note Does NOT work if multiple programs are generated
   */
  void appendEndFor();

  /**
   * Finish current program, a new one is automatically started
   */
  void appendEndProgram(const bool call_next_program);

  /**
   * Append a custom command, the line numbers and end semicolon are automatically added
   */
  void appendCustomCommand(const std::string command);

  bool getPoseFromId(const unsigned pose_id,
                     FanucPose &pose);

private:
  void prependLineNumber(std::string &line,
                         const unsigned number);

  bool tweakProgramName(ProgramName &name,
                        const unsigned number);

  const unsigned program_name_max_size_ = 36;

  std::string permissionsToString(Permissions perm)
  {
    if (perm == READ_WRITE)
      return "READ_WRITE";
    if (perm == READ)
      return "READ";
    else
      return "";
  }

  ProgramName program_name_;

  std::string program_comment_;

  FanucPostProcessor::Permissions permissions_;

  std::string applicative_;

  std::vector<std::shared_ptr<FanucCommand>> commands_;

  std::vector<unsigned> labels_id_;

  bool line_numbers_;

  /**
   * Counts the number of for loops opened
   */
  unsigned for_count_;

  /**
   * Vector containing the poses of our trajectory
   * Not necessarily ordered!
   */
  std::vector<FanucPose> poses_;
};

}
#endif
